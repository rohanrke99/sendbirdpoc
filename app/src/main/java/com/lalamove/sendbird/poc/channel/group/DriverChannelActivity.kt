package com.lalamove.sendbird.poc.channel.group

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.lalamove.sendbird.poc.R
import com.lalamove.sendbird.poc.utils.PreferenceUtils
import com.sendbird.android.GroupChannel

class DriverChannelActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_channel)

        val toolbar = findViewById<Toolbar>(R.id.toolbar_group_channel)
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_24_dp)
        }
       //createDriverChannel()
       createRohanChannel()
        //createManasChannel()
    }


    private fun createDriverChannel(){
        val list = ArrayList<String>()
        list.add(PreferenceUtils.getUserId())
        list.add("rohan.singh@lalamove.com")
        createGroupChannel(list,true)
    }

    private fun createRohanChannel(){
        val list = ArrayList<String>()
        list.add(PreferenceUtils.getUserId())
        list.add("vikram.mittal")
        createGroupChannel(list,true)
    }

    private fun createManasChannel(){
        val list = ArrayList<String>()
        list.add(PreferenceUtils.getUserId())
        createGroupChannel(list,true)
    }

    interface onBackPressedListener {
        fun onBack(): Boolean
    }

    private var mOnBackPressedListener: onBackPressedListener? = null

    fun setOnBackPressedListener(listener: onBackPressedListener) {
        mOnBackPressedListener = listener
    }

    override fun onBackPressed() {
        if (mOnBackPressedListener != null && mOnBackPressedListener!!.onBack()) {
            return
        }
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

     fun setActionBarTitle(title: String) {
        if (supportActionBar != null) {
            supportActionBar!!.title = title
        }
    }


    /**
     * Creates a new Group Channel.
     *
     * Note that if you have not included empty channels in your GroupChannelListQuery,
     * the channel will not be shown in the user's channel list until at least one message
     * has been sent inside.
     *
     * @param userIds   The users to be members of the new channel.
     * @param distinct  Whether the channel is unique for the selected members.
     * If you attempt to create another Distinct channel with the same members,
     * the existing channel instance will be returned.
     */
    private fun createGroupChannel(userIds: List<String>, distinct: Boolean) {
        GroupChannel.createChannelWithUserIds(userIds, distinct,
            GroupChannel.GroupChannelCreateHandler { groupChannel, e ->
                if (e != null) {
                    // Error!
                    return@GroupChannelCreateHandler
                }

                if (groupChannel.url!= null) {
                    // If started from notification
                    val fragment = DriverChatFragment.newInstance(groupChannel.url)
                    val manager = supportFragmentManager
                    manager.beginTransaction()
                        .replace(R.id.container_group_channel, fragment)
                        .addToBackStack(null)
                        .commit()
                }
            })
    }




}
