package com.lalamove.sendbird.poc.utils

import android.content.Context
import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.sendbird.android.SendBird

object PushUtils {

    private const val TAG = "PushUtils"

    fun registerPushTokenForCurrentUser(
        context: Context,
        handler: SendBird.RegisterPushTokenWithStatusHandler
    ) {

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                val token = task.result?.token
                SendBird.registerPushTokenForCurrentUser(token, handler)

            })

    }

    fun unregisterPushTokenForCurrentUser(
        context: Context,
        handler: SendBird.UnregisterPushTokenHandler
    ) {


        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                val token = task.result?.token
                SendBird.unregisterPushTokenForCurrentUser(token, handler)

            })


    }

}
