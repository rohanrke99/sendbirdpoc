package com.lalamove.sendbird.poc

import android.app.Application
import com.lalamove.sendbird.poc.utils.PreferenceUtils
import com.orhanobut.hawk.Hawk
import com.sendbird.android.SendBird


class LalamoveApplication : Application() {

    private val APP_ID = "667B0924-2F42-4800-8955-865F051E39C1" // Lalamove Test


    override fun onCreate() {
        super.onCreate()
       // Hawk.init(this).build()
        PreferenceUtils.init(applicationContext)
        SendBird.init(APP_ID, applicationContext)
    }

    companion object{
        const val VERSION = "3.0.40"
    }

}